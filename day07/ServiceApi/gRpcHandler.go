package ServiceApi

import (
	"code.lhy19970707.com/studygo/GoAdvanced/day07/produc"
	service "code.lhy19970707.com/studygo/GoAdvanced/day07/protoc"
	"context"
	"strconv"
)

type ProdService struct {

}

// GetProdDetail 需要实现的函数 /*
func (p *ProdService)GetProdDetail(ctx context.Context, in *service.ProdsRequest, out *service.ProdListResponse) error{
	/*测试熔断器*/
	//time.Sleep(time.Second*5)
	//
	models:=make([]*service.ProdModel,0)
	models=append(models, produc.NewProd(00+in.ProdId, "SingleProdName"+strconv.Itoa(int(in.ProdId))))
	out.Data=models
	return nil
}

// GetProdsList 需要实现的函数 /*
func (p *ProdService)GetProdsList(ctx context.Context, in *service.ProdsRequest, out *service.ProdListResponse) error{
	models:=make([]*service.ProdModel,0)
	var i int32
	for i=1;i<in.Size;i++ {
		models = append(models, produc.NewProd(100+i, "ProdName"+strconv.Itoa(int(i))))
	}
	out.Data=models
	return nil
}

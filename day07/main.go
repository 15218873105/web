package main

import (
	"code.lhy19970707.com/studygo/GoAdvanced/day07/ServiceApi"
	service "code.lhy19970707.com/studygo/GoAdvanced/day07/protoc"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"log"

)



func main()  {
	etcdReg:=etcd.NewRegistry(registry.Addrs("127.0.0.1:2379"))
	Service:=micro.NewService(
		micro.Name("CreateProdList"),
		micro.Address(":9000"),
		micro.Registry(etcdReg))
	err := service.RegisterProdServiceHandler(Service.Server(),&ServiceApi.ProdService{})
	if err != nil {
		log.Fatal(err)
		return
	}
	Service.Init()
	err = Service.Run()
	if err != nil {
		log.Fatal(err)
		return
	}
}
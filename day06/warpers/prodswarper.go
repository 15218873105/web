package warpers

import (
	"code.lhy19970707.com/studygo/GoAdvanced/day06/produc"
	protoService "code.lhy19970707.com/studygo/GoAdvanced/day06/proto"
	"context"
	"github.com/afex/hystrix-go/hystrix"
	"github.com/micro/go-micro/v2/client"
	"strconv"
)

func defaultProds(req,rsp interface{})  {
	model := make([]*protoService.ProdModel, 0)
	request:=req.(client.Request)
	result:=rsp.(*protoService.ProdListResponse)
	if request.Method()=="ProdService.GetProdsList"{
		var i int32 = 1
		for ; i <= 5; i++ {
			model = append(model, produc.NewProd(i, "Old_ProdName"+strconv.Itoa(int(i))))
		}
		result.Data=model
	}else if request.Method()=="ProdService.GetProdDetail"{
		var id int32=1
		model=append(model, produc.NewProd(id,"Old_ProdName"+strconv.Itoa(int(id))))
		result.Data=model
	}
}


type prodsWrapper struct {
	client.Client
}

func (this *prodsWrapper)Call(ctx context.Context, req client.Request, rsp interface{}, opts ...client.CallOption) error{
	configA:=hystrix.CommandConfig{
		Timeout: 3000,
		//熔断器参数
		//次数
		RequestVolumeThreshold: 4,
		//熔断百分比
		ErrorPercentThreshold: 50,
		//空窗期
		SleepWindow: 5000,
	}
	hystrix.ConfigureCommand("getprods",configA)
	return hystrix.Do("getprods",
		func() error {
			return this.Client.Call(ctx,req,rsp)
		},
		func(err error) error {
			defaultProds(req,rsp)
			return nil
		})

}

/*修饰器调用的方法*/
func NewprodsWrapper(c client.Client)client.Client  {
	return &prodsWrapper{c}
}
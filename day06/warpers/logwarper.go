package warpers
/*4.0 装饰器的使用,就是在client在call之前截断程序再进行补充修饰的操作*/
import (
	"context"
	"fmt"
	"github.com/micro/go-micro/v2/client"
	"github.com/micro/go-micro/v2/metadata"
)

type logWrapper struct {
	client.Client
}

func (this *logWrapper)Call(ctx context.Context, req client.Request, rsp interface{}, opts ...client.CallOption) error{
	//开始截断call方法
	md,_:=metadata.FromContext(ctx)
	fmt.Printf("[log] ctx:%v  service :%v method: %v\n",md,req.Service(),req.Method())
	return this.Client.Call(ctx,req,rsp)
}

/*修饰器调用的方法*/
func NewLogWrapper(c client.Client)client.Client  {
	return &logWrapper{c}
}

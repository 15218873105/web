package main

import (
	"fmt"
	"github.com/micro/go-micro/v2/client/selector"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strings"
	"time"
)



func callAPI(addr string , path string,method string)(string ,error)  {

	//请求参数设定
	form:=url.Values{}
	form.Add("size","1")
	//将参数添加到request中
	request, err := http.NewRequest(method,"http://"+addr+path,strings.NewReader(form.Encode()))

	if err != nil {
		return "", err
	}
	//指定客户端解析request的格式
	request.Header.Add("Content-Type","application/x-www-form-urlencoded")
	client := http.DefaultClient
	do, _ := client.Do(request)
	if err != nil {
		return "", err
	}//通过合成的URL请求页面信息

	all, err := ioutil.ReadAll(do.Body)
	if err != nil {
		return "", err
	}
	return string(all),nil

}


func main()  {
	etcdReg:=etcd.NewRegistry(registry.Addrs("127.0.0.1:2379"))
	service, err := etcdReg.GetService("prodServices")
	if err != nil {
		return
	}
	/*
	随机选择Node和轮询选择Node
	//*/
	//for{
		Next:=selector.Random(service)
		//Next:=selector.Random(service)//Next的类型registry.Node
		Node,err:=Next()//Next是个函数，直接执行可获得随机选择的节点的结构体
		if err!=nil{
			log.Fatal(err)
		}
		fmt	.Printf("Get Msg from luohaoyuan %v,%v,%v\n",Node.Address,Node.Id,Node.Metadata)
		time.Sleep(time.Second*1)
		msg, err := callAPI(
			Node.Address,
			"/v1/user",
			http.MethodPost,
		)
		if err != nil {
		return
		}
		fmt.Print(msg)
	//}
	//Get Msg from luohaoyuan 172.20.10.2:8001,bb3d9357-bb34-4051-8c75-6671f7c996a9,map[]
}

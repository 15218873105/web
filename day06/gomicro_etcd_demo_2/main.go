package main

import (
	"context"
	"fmt"

	"github.com/micro/go-micro/v2/client"
	"github.com/micro/go-micro/v2/client/selector"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"github.com/micro/go-micro/v2/util/log"
)

func callAPI2(s selector.Selector){

	Webclient :=client.NewClient(
		client.Selector(s),
		/*
		"application/grpc":
		"application/grpc+json":
		"application/grpc+proto":
		"application/protobuf":
		"application/json":
		"application/json-rpc":
		"application/proto-rpc":
		"application/octet-stream": */
		client.ContentType("application/json"),
		)

	req:= Webclient.NewRequest("prodService","/v1/user",map[string]string{"size":"4"})
	//根据目标服务所返回的数据结构来定义接受的变量类型
	var rsp map[string]interface{}

	err := Webclient.Call(context.Background(), req, &rsp) //需要传入传出参数
	if err != nil {
		log.Fatal(err)
	}
	fmt.Print(rsp["data"])


}


func main()  {
	etcdReg:=etcd.NewRegistry(registry.Addrs("127.0.0.1:2379"))
	s:=selector.NewSelector(
		selector.Registry(etcdReg),
		)
	callAPI2(s)
}

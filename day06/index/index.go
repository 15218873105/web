package main

import (
	"github.com/gin-gonic/gin"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"github.com/micro/go-micro/v2/web"
	"net/http"

)


func hello(context *gin.Context)  {
	context.String(200,"hello api")
}

//主站
func main()  {
	etcdReg:=etcd.NewRegistry(
		registry.Addrs("127.0.0.1:2379"),
	)
	ginRouter:=gin.Default()
	ginRouter.Handle(http.MethodGet,"/",hello)
	service :=web.NewService(
		web.Name("luohaoyuan"),
		web.Address(":8001"),
		web.Handler(ginRouter),
		web.Registry(etcdReg),
	)
	//service.Init()
	err := service.Run()
	if err != nil {
		return
	}
}


package main

import (
	protoService "code.lhy19970707.com/studygo/GoAdvanced/day06/proto"
	"code.lhy19970707.com/studygo/GoAdvanced/day06/warpers"
	Router "code.lhy19970707.com/studygo/GoAdvanced/day06/webService"
	"fmt"
	"github.com/micro/go-micro/v2"
	"github.com/micro/go-micro/v2/registry"
	"github.com/micro/go-micro/v2/registry/etcd"
	"github.com/micro/go-micro/v2/web"
)

/*
1.0
*/
//func SenMsg(context *gin.Context)  {
//context.JSON(200,gin.H{
//	"data":"index",
//})//gin.H的作用是生成一个JSON字段，因为在gin.H内部是一个map[string]interface
//}
/*2.0*/
//
//func sendMsg(context *gin.Context)  {
//	var pr helper.ProdSize
//
//	//gin框架通过reflect解析context中的成员变量
//	err := context.Bind(&pr)
//	if err != nil{
//		log.Fatal(err)
//	}
//	//context.JSON(200, produc.NewProdList(5))
//	context.JSON(
//		200,gin.H{
//			"data":produc.NewProdList(pr.Size),//NewProdList返回的是一个列表
//		})
//}
/*3.0*/





//负载均衡使用的从站，既可以通过主站访问，也可以直接访问
func main( )  {
	etcdReg:=etcd.NewRegistry(
		registry.Addrs("127.0.0.1:2379"),
	)

	/*使用go-micro创建micro的服务（）*/
	createProds:=micro.NewService(
		micro.Name("CreateProdList_client"),

		micro.WrapClient(warpers.NewLogWrapper), //修饰器传入grpc，每次Grpc调用Call时输出日志

		micro.WrapClient(warpers.NewprodsWrapper), //修饰器传入grpc，每次Grpc调用hystrix熔断器
		)
	/*将创建的micro服务的客户端封装为prodService类型*/
	grpcProds:= protoService.NewProdService(
		"CreateProdList",createProds.Client(),
		)
	/*将封装好的prodService类型绑定到路由中*/
	//Router.InitMiddleware(grpcProds)
	/*使用go-micro创建web服务，并且绑定到etcd中*/
	service :=web.NewService(
		web.Name("prodServices"),
		web.Address(":8011"),
		web.Handler(Router.Router(grpcProds)),
		web.Registry(etcdReg),
		)


	service.Init()
	err := service.Run()
	if err != nil {
		fmt.Print(err)
		return
	}
}

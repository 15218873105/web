package produc

import (
	protoService "code.lhy19970707.com/studygo/GoAdvanced/day06/proto"
)

type ProdModel struct {
	ProdID int
	ProdName string
}

func NewProd(id int32  ,pname string)*protoService.ProdModel {
	return &protoService.ProdModel{
		ProdID:  id,
		ProName: pname,
	}
}

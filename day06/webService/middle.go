package webService

import (
	protoService "code.lhy19970707.com/studygo/GoAdvanced/day06/proto"
	"github.com/gin-gonic/gin"
)
/*InitMiddleware 使用map传递protoService.ProdService*/
func InitMiddleware(service protoService.ProdService) gin.HandlerFunc {
	return func(context *gin.Context) {
		context.Keys=make(map[string]interface{})
		context.Keys["grpcProds"]=service
		context.Next()//调用后续的逻辑函数
		//context.Abort()//阻止后续逻辑函数的运行
	}
}
func ErrorMiddleware()gin.HandlerFunc  {
	return func(context *gin.Context) {
		defer func() {
			if r:=recover();r!=nil{
				context.JSON(500,gin.H{"Status":r})
				context.Abort()
			}
		}()
		context.Next()
	}
}

package webService

import (
	protoService "code.lhy19970707.com/studygo/GoAdvanced/day06/proto"
	"github.com/gin-gonic/gin"
	"net/http"
)

func Router(service protoService.ProdService)*gin.Engine  {
	ginRouter:=gin.Default()
	//将修饰器装载到gin-路由上面，
	//当有客户端访问时会先处理绑定的修饰器
	ginRouter.Use(InitMiddleware(service), ErrorMiddleware()) //

	v1Group:=ginRouter.Group("/v1")
	{
		v1Group.Handle(http.MethodPost,"/prods", GetProdsList)
		v1Group.Handle(http.MethodGet,"/prods/Detail", GetProdDetail)
	}
	return ginRouter
}

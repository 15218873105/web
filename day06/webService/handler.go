package webService

import (
	"code.lhy19970707.com/studygo/GoAdvanced/day06/helper"
	protoService "code.lhy19970707.com/studygo/GoAdvanced/day06/proto"
	"context"
	"github.com/gin-gonic/gin"
	"strconv"
)

func GetProdDetail(ginctx *gin.Context)  {
	grpcProds:=ginctx.Keys["grpcProds"].(protoService.ProdService) //断言判断类型
	var pr helper.Prodid
	ginctx.Bind(&pr)
	id,err:=strconv.Atoi(pr.ProdId)
	if err!=nil || id <=0{
		id=1
	}
	proReq:= protoService.ProdsRequest{ProdId: int32(id)}
	proRes, err := grpcProds.GetProdDetail(context.Background(), &proReq)
	if err!=nil{
		ginctx.JSON(500,gin.H{"Status":err})
	}else{
		ginctx.JSON(200,gin.H{"data":proRes.Data})
	}
}



func GetProdsList(ginctx *gin.Context)  {
		var List *protoService.ProdListResponse
		grpcProds:=ginctx.Keys["grpcProds"].(protoService.ProdService) //断言判断类型
		var pr helper.ProdSize
		ginctx.Bind(&pr)
		size,err:=strconv.Atoi(pr.Size)
		if err!=nil || size < 0{
			size=2
		}else{
			prodReq:= protoService.ProdsRequest{Size: int32(size)}
			//使用micro内置的grpc远程调用函数grpcProds
			List,err=grpcProds.GetProdsList(context.Background(),&prodReq)
			if err!=nil{
				ginctx.JSON(500,gin.H{"Status":err})
			}else{
				ginctx.JSON(200,gin.H{"data":List})
			}



			//******************//
			/*
			通过go-micro框架将hystrix整合进修饰器中，
			在handler中避免出现过多判断语句
			*/

			///*
			//1-熔断器处理命令行模式，需要配置文件
			//*/
			//configA:=hystrix.CommandConfig{
			//	Timeout: 3000,//毫秒
			//}
			///*
			//2-使用配置文件配置command
			//*/
			//hystrix.ConfigureCommand("getprods",configA)//名字随便
			//
			///*
			//3-执行使用Do方法
			//*/
			//
			//err=hystrix.Do("getprods", func() error {
			//	prodReq:=protoService.ProdsRequest{Size:int32(size)}
			//	//通过micro使用grpc调用远程服务的函数，生成并返回生成的内容
			//	List,err=grpcProds.GetProdsList(context.Background(),&prodReq)
			//	return err//如果有err，会传递到Do方法返回的err中，如果超时Do方法同样会产生err
			//}, func(err error) error {
			//	model:=make([]*protoService.ProdModel,0)
			//	var i int32=0
			//	for ;i<=5;i++{
			//		model=append(model,produc.NewProd(i,"Old_ProdName"+strconv.Itoa(int(i))))
			//	}
			//	/*一定要重新定义Response变量，
			//	不能直接更改List的Data成员，
			//	因为其其他成员依然有错误*/
			//	res:=&protoService.ProdListResponse{}
			//	res.Data=model
			//	List=res
			//	return err
			//})//fallback是降级处理函数的实现位置
			//	ginctx.JSON(200,gin.H{"data":List})



		}

	}

